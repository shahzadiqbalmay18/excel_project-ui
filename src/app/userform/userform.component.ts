import { Component, OnInit } from '@angular/core';
import { Data } from './data';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { DataserviceService } from '../Services/dataservice.service';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.css']
})
export class UserformComponent implements OnInit {
  converteddate:any;
  value: boolean = false;
  data: Data = new Data();
  id: any;
  dateoffc:Date;
  checkinDate:Date;
  checkoutDate:Date;
  status1: SelectItem[];  
  status2:SelectItem[];  
  status3:SelectItem[];  
  title:SelectItem[];
  nationality:SelectItem[];
  countryofresidence:SelectItem[];
  firstpointofcontact:SelectItem[];
  


  
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private dataService: DataserviceService
  ) {

    this.nationality=[
      {label:'Select Nationality', value:null},
      {label:'Ghanaian', value:'Ghanaian'},
      {label:'Non-Ghanaian ', value:'Non-Ghanaian '},
      
    ];
this.firstpointofcontact=[
  {label:'First Point of Contact', value:null},
  {label:'Office Visit ', value:'Office Visit '},
  {label:'Website Registration ', value:'Website Registration '},
  {label:'Email Enquiry ', value:'Email Enquiry '},
  {label:'Phone Enquiry ', value:'Phone Enquiry '},
  {label:'Event Registration ', value:'Event Registration '},
  {label:'Agent Introduction ', value:'Agent Introduction '},
  {label:' Client Referral ', value:'Client Referral  '},
]
    
    
this.status1 = [
  {label:'Primary Status', value:null},
  {label:'Agent ', value:'Agent '},
  {label:'Ex Buyer (Withdrew)', value:'Ex Buyer (Withdrew)'},
  {label:'Tenant', value:'Tenant'},
  {label:'Supplier ', value:'Supplier '},
  {label:'Prospect ', value:'Prospect '},
  {label:'Buyer ', value:'Buyer '},
  {label:'Short Term Tenant ', value:'Short Term Tenant '},
  {label:'Long Term Tenant ', value:'Long Term Tenant '},
];
    
this.status2 = [
  {label:'Second Status', value:null},
  {label:'Agent ', value:'Agent '},
  {label:'Ex Buyer (Withdrew)', value:'Ex Buyer (Withdrew)'},
  {label:'Tenant', value:'Tenant'},
  {label:'Supplier ', value:'Supplier '},
  {label:'Prospect ', value:'Prospect '},
  {label:'Buyer ', value:'Buyer '},
  {label:'Short Term Tenant ', value:'Short Term Tenant '},
  {label:'Long Term Tenant ', value:'Long Term Tenant '},
];

    this.status3 = [
      {label:'Third Status', value:null},
      {label:'Agent ', value:'Agent '},
      {label:'Ex Buyer (Withdrew)', value:'Ex Buyer (Withdrew)'},
      {label:'Tenant', value:'Tenant'},
      {label:'Supplier ', value:'Supplier '},
      {label:'Prospect ', value:'Prospect '},
      {label:'Buyer ', value:'Buyer '},
      {label:'Short Term Tenant ', value:'Short Term Tenant '},
      {label:'Long Term Tenant ', value:'Long Term Tenant '},
  ];
  this.title=[
    {label:'Select Title', value:null},
    {label:'Mr.', value:'Mr'},
    {label:'Mrs.', value:'Mrs'},
  ];

    console.log(this.data);
  }

  ngOnInit() {

this.addinddataindropdown();


    this.id = this.activatedRoute.snapshot.params['id'];
    if (this.id) {
      this.getbyid(this.id);
    }
  }

  addinddataindropdown(){
   
let array1:SelectItem[]=[];
    let array:any[];
  array=['Ghana', 
      'Nigeria',
      'USA',
      'UK',
      'Albania',
      'Algeria',
      'Angola',
      'Argentina',
      'Armenia',
      'Australia',
      'Austria',
      'Azerbaijan',
      'Bangladesh',
      'Belarus',
      'Belgium',
      'Benin',
      'Bolivia',
      'Bosnia and Herzegovina',
      'Botswana',
      'Brazil',
      'Bulgaria',
      'Burkina Faso',
      'Burma',
      'Cambodia',
      'Cameroon',
      'Canada',
      'Central African Republic',
      'Chad',
      'Chile',
      'China',
      'Colombia',
      'Costa Rica',
      'Croatia',
      'Cuba',
      'Czech Republic',
      'Democratic Republic of the Congo',
      'Denmark',
      'Dominican Republic',
      'Ecuador',
      'Egypt',
      'El Salvador',
      'Equatorial Guinea',
      'Eritrea',
      'Estonia',
      'Ethiopia',
      'Finland',
      'France',
      'Gabon',
      'Gambia',
      'Germany',
      'Ghana',
      'Greece',
      'Guatemala',
      'Guinea',
      'Guinea-Bissau',
      'Honduras',
      'Hong Kong (China)',
      'Hungary',
      'India',
      'Indonesia',
      'Iran',
      'Iraq',
      'Ireland',
      'Israel',
      'Italy',
      'Ivory Coast',
      'Jamaica',
      'Japan',
      'Kazakhstan',
      'Kenya',
      'Kosovo',
      'Kuwait',
      'Latvia',
      'Lebanon',
      'Lesotho',
      'Liberia',
      'Libya',
      'Lithuania',
      'Macedonia',
      'Madagascar',
      'Malawi',
      'Malaysia',
      'Mali',
      'Mauritania',
      'Mauritius',
      'Mexico',
      'Moldov',
      'Mongolia',
      'Morocco',
      'Mozambique',
      'Namibia',
      'Netherlands',
      'New Zealand',
      'Niger',
      'Nigeria',
      'North Korea',
      'Norway',
      'Oman',
      'Pakistan',
      'Palestine',
      'Panama',
      'Paraguay',
      'Peru',
      'Philippines',
      'Poland',
      'Portugal',
      'Puerto Rico',
      'Qatar',
      'Republic of the Congo',
      'Romania',
      'Russia',
      'Rwanda',
      'Saudi Arabia',
      'Senegal',
      'Serbia',
      'Sierra Leone',
      'Singapore',
      'Slovakia',
      'Slovenia',
      'Somalia',
      'South Africa',
      'South Korea',
      'South Sudan',
      'Spain',
      'Sri Lanka',
      'Sudan',
      'Sweden',
      'Switzerland',
      'Syria',
      'Taiwan',
      'Tanzania',
      'Thailand',
      'Togo',
      'Trinidad and Tobago',
      'Tunisia',
      'Turkey',
      'Turkmenistan',
      'Uganda',
      'Ukraine',
      'United Arab Emirates',
      'United Kingdom',
      'United States',
      'Uruguay',
      'Venezuela',
      'Vietnam',
      'Zambia',
      'Zimbabwe',
      'Russia',
      'Rwanda',
      'Saint Kitts and Nevis',
      'Saint Lucia',
      'Saint Vincent and the Grenadines',
      'Samoa',
      'San Marino',
      'São Tomé and Príncipe',
      'Saudi Arabia',
      'Senegal',
      'Serbia',
      'Seychelles',
      'Sierra Leone',
      'Singapore',
      'Slovakia',
      'Slovenia',
      'Solomon Islands',
      'Somalia',
      'South Africa',
      'South Sudan',
      'Spain',
      'Sri Lanka',
      'Sudan',
      'Suriname',
      'Swaziland',
      'Sweden',
      'Switzerland',
      'Syria',
      'Tajikistan',
      'Tanzania',
      'Thailand',
      'Timor-Leste[18]',
      'Togo',
      'Tokelau',
      'Tonga',
      'Trinidad and Tobago',
      'Tunisia',
      'Turkey',
      'Turkmenistan',
      'Tuvalu',
      'Uganda',
      'Ukraine',
      'United Arab Emirates',
      'United Kingdom of Great Britain and Northern Ireland', 
      'United States of America',
      'Uruguay',
      'Uzbekistan',
      'Vanuatu',
      'Vatican City State',
      'Venezuela',
      'Vietna',
      'Yemen',
      'Zambia',
      'Zimbabwe'];

      array1.push({label:'Select Country of Residence',value:null})
      array.forEach(function(currentValue){
        array1.push({label:currentValue,value:currentValue});

      
        
      
        
      }

     
      
      )

      this.countryofresidence=array1;
      
  
  
  
    }



  private getbyid(id:any) {
    this.dataService.getdatabyid(id).subscribe(d => {
      console.log(d);
      
      this.data.name = d.name;
      this.data.title=d.title;
      this.data.surname=d.surname;
      this.data.email1 = d.email1;
      this.data.email2 = d.email2;
      this.data.phoneNo1 = d.phoneNo1;
      this.data.phoneNo2 = d.phoneNo2;
      this.data.company = d.company;
      this.data.nationality = d.nationality;
      this.data.countryOfResidence = d.countryOfResidence;
      this.data.status1 = d.status1;
      this.data.status2 = d.status2;
      this.data.status3 = d.status3;
      this.dateoffc=new Date(d.dateOffc);
      this.checkinDate = new Date(d.checkinDate);
      this.checkoutDate = new Date(d.checkoutDate);
      this.data.daysOfRental = d.daysOfRental;
      this.data.firstPointOfContact = d.firstPointOfContact;
      this.data.details = d.details;
      this.data.oEmbassyGardens = d.oEmbassyGardens;
      this.data.oTheGallery = d.oTheGallery;
      this.data.oTheResidence = d.oTheResidence;
      this.data.oCliftonCourt = d.oCliftonCourt;
      this.data.oCliftonPlace = d.oCliftonPlace;
      this.data.oKaiVillas = d.oKaiVillas;
      this.data.oAddyVillas = d.oAddyVillas;
      this.data.total = d.total;
      this.data.tEmbassyGardens = d.tEmbassyGardens;
      this.data.tTheGallery = d.tTheGallery;
      this.data.tTheResidence = d.tTheResidence;
      this.data.tCliftonCourt = d.tCliftonCourt;
      this.data.tCliftonPlace = d.tCliftonPlace;
      this.data.tKaiVillas = d.tKaiVillas;
      this.data.tAddyVillas = d.tAddyVillas;
      this.data.oLennox=d.oLennox;
      this.data.tLennox=d.tLennox;
    });
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  changedatetostring(date: Date) {
    if(date==null ){
      return "";
    }
    else{

   
    this.converteddate = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    return this.converteddate;
    }
  }

  save() {

    if(this.data.total==null){
      this.data.total=0;
      

    }

    console.log(this.data);
   // this.data.dateOffc = this.data.dateOffc.toString();

    if (this.id != 0 && this.id != null) {
      this.data.dateOffc=this.changedatetostring(this.dateoffc);
      this.data.checkinDate = this.changedatetostring(this.checkinDate);
      this.data.checkoutDate = this.changedatetostring(this.checkoutDate);
      this.dataService.updatedatabyid(this.data,this.id).subscribe(
        response => {
          console.log('', response);
          this.messageService.add({
              severity: 'success',
            summary: 'Data updated successfully',
            detail: 'Added'
          });
        },
        error => {
          this.messageService.add({
           severity: 'error',
            summary: 'Failed',
            detail: 'Something Went Wrong'
          });
        }
      );
    } else {
      console.log(this.data.total);
      this.data.dateOffc=this.changedatetostring(this.dateoffc);
      this.data.checkinDate = this.changedatetostring(this.checkinDate);
      this.data.checkoutDate = this.changedatetostring(this.checkoutDate);
      console.log(this.data);
      this.dataService.savedata(this.data).subscribe(
        d => {
          console.log('', d);
          this.messageService.add({
          
            severity: 'success',
            summary: 'Data saved successfully',
            detail: 'Added'
          });
        },
        error => {
          this.messageService.add({
          
            severity: 'error',
            summary: 'Failed',
            detail: 'Unable to add Data'
          });
        }
      );
    }
  }

  onclick() {
    this.save();
  }

  routePage() {
    this.router.navigate(['']);
  }

  show() {
    console.log(this.data.oEmbassyGardens);
  }
}
